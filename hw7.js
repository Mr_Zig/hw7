var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];
//Задание 1
var students = [],
    points = [],
    maxPoint,
    maxPointIndex,
    pointIndex;
studentsAndPoints.forEach(function(item, i, studentsAndPoints) {
  if (i % 2) {
    points.push(studentsAndPoints[i]);
    pointIndex=i;
  }
  else {
    students.push(studentsAndPoints[i]);
    studentIndex=i;
  }
});
// console.log (students);
// console.log (points);

//Задание 2
console.log('Список студентов:');
students.forEach(function (student, i){
  console.log('Студент '+student+' набрал '+points[i]+' баллов')
})
//Задание 3
points.forEach(function (point, i) {
  if (!maxPoint || point > maxPoint) {
  maxPoint = point;
  maxPointIndex = i;
}
})
console.log('Студент набравший максимальный балл: '+students[maxPointIndex]+' ('+maxPoint+' баллов)');
//Задание 4
var addPoint = points.map(function (point, i) {
if (i == students.indexOf('Ирина Овчинникова') || i == students.indexOf('Александр Малов')) {
 point += 30;
}
 return point;
});
points=addPoint;
//Для проверки:
// console.log (students);
// console.log (points);

//Дополнительно задание
// Пока не готово
// function comparePoint(a, b) {
//   if (a > b) return -1;
//   if (a < b) return 1;
// }
// points.sort(comparePoint);
// console.log(points);


// function getTop(n) {
//   console.log('Топ '+n+':');
//   students.forEach(function (item, i){
//     if (n <= i) {
//       return i;
//     }
//     console.log(''+students[i]+' — '+points[i]+' баллов');
//   });

// };
// getTop(5);

